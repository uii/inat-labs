#include <cstring>
#include <iostream>
#include <string>

#include "../Common/networking.h"

void communicate(SOCKET channel);

sockaddr_in input_endpoint(const char* for_what);
void print_error(const char* where);

int main()
{
    if (NET_initialize())
    {
        print_error("NET_initialize");
    }
    auto channel = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (channel)
    {
        communicate(channel);
        ::closesocket(channel);
    }
    NET_finalize();
    return 0;
}

void communicate(SOCKET channel)
{
    int yes = 1;
    int result = ::setsockopt(channel,
                              SOL_SOCKET,
                              SO_REUSEADDR,
                              (const char*)&yes, sizeof(yes));
    result = ::setsockopt(channel,
                          SOL_SOCKET,
                          SO_BROADCAST,
                          (const char*)&yes, sizeof(yes));

    sockaddr_in const own_endpoint = input_endpoint("own");
    result = ::bind(channel, (sockaddr*)&own_endpoint, sizeof(own_endpoint));
    if (result)
    {
        print_error("bind");
        return;
    }

    do
    {
        char choice;
        std::cout << "\nType `S' to send message, `R' to receive, any other key to quit: ";
        std::cin >> choice;

        sockaddr_in endpoint;
        socklen_t size;
        char buffer[512];
        switch (choice)
        {
        case 'R':
        case 'r':
            size = sizeof(endpoint);
            result = ::recvfrom(channel,
                                buffer, sizeof(buffer),
                                0,
                                (sockaddr*)&endpoint, &size);
            if (result > 0)
            {
                std::cout << ::inet_ntoa(endpoint.sin_addr) << ":"
                          << ::ntohs(endpoint.sin_port) << ":\n\t"
                          << buffer << std::endl;
            }
            else if (result < 0)
            {
                print_error("recvfrom");
                return;
            }
            else
            {
                std::cout << "Empty datagram received from "
                          << ::inet_ntoa(endpoint.sin_addr) << ":"
                          << ::ntohs(endpoint.sin_port) << "." << std::endl;
            }
            break;

        case 'S':
        case 's':
            endpoint = input_endpoint("target");
            std::cout << "Enter message to send: ";
            std::cin.getline(buffer, sizeof(buffer)); // Ruthlessly eat line delimiter.
            std::cin.getline(buffer, sizeof(buffer));
            result = ::sendto(
                         channel, buffer, std::strlen(buffer) + 1,
                         0,
                         (const sockaddr*)&endpoint, sizeof(endpoint));
            if (result > 0)
            {
                std::cout << "Sent " << result << " octets." << std::endl;
            }
            else
            {
                print_error("sendto");
                return;
            }
            break;

        default:
            std::cout << "Quiting." << std::endl;
            return;
        }
    }
    while (true);
}

void print_error(const char* where)
{
    int const error_code = NET_get_last_error();
    std::cerr << "ERROR: " << where << "(), code " << error_code << "!" << std::endl;
}

sockaddr_in input_endpoint(const char* for_what)
{
    sockaddr_in result;
    result.sin_family = AF_INET;
    std::cout << "Specify " << for_what << " endpoint:\n";

    char ip_address_string[16];
    std::cout << "\tHost: ";
    std::cin >> ip_address_string;
    result.sin_addr.s_addr = ::inet_addr(ip_address_string);

    unsigned short port;
    std::cout << "\tPort: ";
    std::cin >> port;
    result.sin_port = ::htons(port);

    return result;
}
