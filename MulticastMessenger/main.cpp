#include <cstring>
#include <iostream>
#include <thread>

#include "../Common/networking.h"
#include "../Common/NetworkError.h"

#include "multicast_messenger.h"
#include "message.h"

const char QUIT_COMMAND[] = "/quit";

void print_messege(const Message& msg, const std::string& own_nickname) {
	bool const needs_display = msg.type == Text
                            && msg.nickname != own_nickname
                            && msg.nickname.length() > 0
                            && msg.text != QUIT_COMMAND;
    if (needs_display) {
		// NOTE: On Windows, using <iostream> here corrupts caret position.
		std::printf("%s: %s\n", msg.nickname.c_str(), msg.text.c_str());
	}
}

void incoming_process(MulticastMessenger& messenger, bool& stop) {
	while (!stop) {
		messenger.process_next_messege(print_messege, 50000);
	}
}

int main(int argc, char* argv[]) {
	try {
	    NET_initialize();

	    char local_address[INET_ADDRSTRLEN + 1] { '\0' };
	    char group_address[INET_ADDRSTRLEN + 1] { '\0' };
	    uint16_t port;

	    if (argc == 4) {
            std::strncpy(local_address, argv[1], INET_ADDRSTRLEN);
            std::strncpy(group_address, argv[2], INET_ADDRSTRLEN);
            port = std::atoi(argv[3]);
	    } else {
            std::cout << "Enter local IP address: ";
            std::cin >> local_address;
            std::cout << "Enter multicast group IP address: ";
            std::cin >> group_address;
            std::cout << "Enter port number: ";
            std::cin >> port;
	    }

		MulticastMessenger messenger(local_address, group_address, port);

		std::string input;
		std::cout << "Enter nickname: ";
		std::cin >> input;

		while(!messenger.set_nickname(input)) {
			std::cout << "Sorry, this nickname is already in use, please enter another: ";
			std::cin >> input;
		}
		std::cout
            << "Welcome, " << input << "! Please feel free to chat from now.\n"
			<< "Enter \"" << QUIT_COMMAND << "\" to quit.\n";

		bool stop = false;
		std::thread thr(incoming_process, std::ref(messenger), std::ref(stop));

		std::getline(std::cin, input);
		do {
			std::getline(std::cin, input);
			if (input == QUIT_COMMAND) {
				stop = true;
				messenger.shutdown();
				break;
			}
			messenger.send_messege(input);
		} while (true);
        thr.join();
	} catch (const NetworkError& error) {
        std::cerr << "Network ERROR "
                  << error.getCode() << ": " << error.getMessage() << '\n'
                  << "AT " << error.getFile() << ": " << error.getLine() << '\n'
                  << "IN " << error.getPlace() << std::endl;
	} catch (...) {
		std::cerr << "Unexpected ERROR!" << std::endl;
	}

	NET_finalize();

	return 0;
}

