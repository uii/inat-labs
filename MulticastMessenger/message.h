#pragma once
#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <iostream>
#include <string.h>

enum MessageType {
    Text,
    NicknameRequest,
    NicknameInUse,
	Bad
};

struct Message {
    MessageType type;
    std::string nickname;
    std::string text;

	Message() {}
	Message(MessageType type, const std::string& nickname, const std::string& text);
	~Message() {}
	std::string encode() const;
    void decode(const std::string& encoded_message);
};

#endif //MESSAGE_H
