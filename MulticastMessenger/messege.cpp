#include "message.h"

Message::Message(MessageType type, const std::string& nickname, const std::string& text) :
	type(type),
	nickname(nickname),
	text(text)
{}

std::string Message::encode() const {
	std::string metadata;
	metadata.resize(sizeof(MessageType) + sizeof(int));
	unsigned int nickname_size = nickname.size();
	strncpy(&metadata[0], (char*)&type, sizeof(MessageType));
	strncpy(&metadata[0] + sizeof(MessageType), (char*)&nickname_size, sizeof(int));

	return metadata + nickname + text;
}

void Message::decode(const std::string& encoded_message) {
    if (encoded_message.size() < sizeof(MessageType) + sizeof(int)) {
        type = Bad;
    }
	type = *(MessageType*)&encoded_message[0];
	unsigned int nickname_size = *(unsigned int*)&encoded_message[sizeof(MessageType)];
	nickname = encoded_message.substr(sizeof(MessageType) + sizeof(int), nickname_size);
	text = encoded_message.substr(
		sizeof(MessageType) + sizeof(int) + nickname_size,
		encoded_message.size() - (sizeof(MessageType) + sizeof(int) + nickname_size)
	);
}
