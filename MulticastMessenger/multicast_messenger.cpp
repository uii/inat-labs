#include <ctime>

#include "../Common/NetworkError.h"

#include "multicast_messenger.h"

MulticastMessenger::MulticastMessenger(
        const char* interface_address,
        const char* group_address,
        int16_t port)
{
	m_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (NET_is_invalid(m_socket)) {
		RAISE_NETWORK_ERROR();
	}

	int reuse = 1;
	if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0) {
		closesocket(m_socket);
		RAISE_NETWORK_ERROR();
	}

	sockaddr_in localSock;
	localSock.sin_family = AF_INET;
	localSock.sin_port = htons(port);
	localSock.sin_addr.s_addr = INADDR_ANY;

	if(bind(m_socket, (struct sockaddr*)&localSock, sizeof(localSock)))	{
		closesocket(m_socket);
		RAISE_NETWORK_ERROR();
	}

	ip_mreq group;
	group.imr_multiaddr.s_addr = inet_addr(group_address);
	group.imr_interface.s_addr = inet_addr(interface_address);
	if(setsockopt(m_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0) {
		closesocket(m_socket);
		RAISE_NETWORK_ERROR();
	}

	m_group_socket.sin_family = AF_INET;
	m_group_socket.sin_addr.s_addr = inet_addr(group_address);
	m_group_socket.sin_port = htons(port);
}

MulticastMessenger::~MulticastMessenger()
{
	closesocket(m_socket);
}

bool MulticastMessenger::set_nickname(const std::string& nickname)
{
	Message msg;
	msg.type = NicknameRequest;
	msg.nickname = nickname;

	send_messege(msg);
	bool nickname_avalible = true;
	time_t timer = time(NULL);
	do {
		msg = get_messege(TIMEOUT_NICKNAME_VERIFICATION);
		if (msg.type == NicknameInUse && msg.nickname == nickname) {
			nickname_avalible = false;
		}
	} while (nickname_avalible && difftime(time(NULL), timer) * 1000 < TIMEOUT_NICKNAME_VERIFICATION);

	if (nickname_avalible) {
		m_nickname = nickname;
	}

	return nickname_avalible;
}

void MulticastMessenger::send_text(const std::string& text)
{
	if (sendto(	m_socket, text.c_str(),
				text.size(), 0, (struct sockaddr*)&m_group_socket,
				sizeof(m_group_socket)) < 0) {
		RAISE_NETWORK_ERROR();
	}
}

void MulticastMessenger::send_messege(const std::string& text)
{
	if (m_nickname.empty()) {
	}
	Message msg;
	msg.text = text;
	msg.nickname = m_nickname;
	msg.type = Text;

	send_messege(msg);
}

void MulticastMessenger::send_messege(const Message& message)
{
    send_text(message.encode());
}

void MulticastMessenger::process_next_messege(
        void (*callback)(const Message&, const std::string&),
        unsigned int timeout)
{
	Message msg = get_messege(timeout);
	if (msg.type == NicknameRequest && msg.nickname == m_nickname) {
		msg.type = NicknameInUse;
		send_messege(msg);
	}
	if (callback != NULL) {
		(*callback)(msg, m_nickname);
	}
}

void MulticastMessenger::shutdown()
{
    ::shutdown(m_socket, SD_BOTH);
    ::closesocket(m_socket);
}

Message MulticastMessenger::get_messege(unsigned int timeout)
{
	Message msg;
	std::string text = get_text(timeout);
	if (text.size() > 0) {
		msg.decode(text);
	} else {
		msg.type = Bad;
	}
	return msg;
}

std::string MulticastMessenger::get_text(unsigned int timeout)
{
#ifdef NET_OS_UNIX
	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = timeout * 1000;
#else
    DWORD const tv = timeout;
#endif
	setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));

	std::string result;
	result.resize(MAX_MESSEGE_SIZE);
	if(recv(m_socket, (char*)&result[0], result.size(), 0) < 0) {
		return "";
	}
	return result;
}
