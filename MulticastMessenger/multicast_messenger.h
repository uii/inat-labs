#ifndef MULTICAST_MESSENGER
#define MULTICAST_MESSENGER

#include "../Common/networking.h"
#include "message.h"

const unsigned int TIMEOUT_NICKNAME_VERIFICATION = 100; //ms
const unsigned int MAX_MESSEGE_SIZE = 1024;

class MulticastMessenger final {
public:
	MulticastMessenger(
        const char* interface_address,
        const char* group_address,
        int16_t port);
	~MulticastMessenger();

	bool set_nickname(const std::string& nickname);
	void send_messege(const std::string& text);
	// timeout in milliseconds
	void process_next_messege(
        void (*callback)(const Message&, const std::string&) = NULL,
        unsigned int timeout = 0);
    void shutdown();

private:
	// get messege text "as is"
	std::string get_text(unsigned int timeout = 0);
	void send_text(const std::string& text);
	// encode messege and send it
    void send_messege(const Message& message);
	Message get_messege(unsigned int timeout = 0);

	std::string m_nickname;
	SOCKET m_socket;
    sockaddr_in m_group_socket;
};

#endif // MULTICAST_MESSENGER
