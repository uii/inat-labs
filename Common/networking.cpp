#include "networking.h"

sockaddr_in make_endpoint(const char* host, unsigned short port)
{
    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = ::inet_addr(host);
    address.sin_port = ::htons(port);
    return address;
}
