#ifndef FILE_SYS_TOOLS_H
#define FILE_SYS_TOOLS_H
#pragma once
//======================================================================
//#define _SVID_SOURCE
#include <dirent.h>
#include <cstring>
#include <stdio.h>
#include <cstdlib>
#include <string>
#include <vector>
//======================================================================

//Возвращает вектор строк с названием файлов, которые содержатся в данной директории
const std::vector<std::string> scan_dir(const char* dir)
{
	std::vector<std::string> result;
	struct dirent **namelist;
	const int n = scandir(dir, &namelist, NULL, alphasort);
	if(n > 0){
		for(size_t i = 0; i < n; ++i){
			if(namelist[i]->d_type == DT_REG)
				result.push_back(std::string(namelist[i]->d_name));
			free(namelist[i]);
		}
		free(namelist);
	}
	return result;
}

const std::vector<std::string> scan_dir(const std::string& dir)
{
	return scan_dir(dir.c_str());
}

//Проверяет существует ли файл
#include <unistd.h>

bool file_is_exist (const char* name)
{
	return (access(name, F_OK) != true);
}

bool file_is_exist (const std::string& name)
{
	file_is_exist(name.c_str());
}


#endif // FILE_SYS_TOOLS_H
