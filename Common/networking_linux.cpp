#include "networking.h"

#include <cerrno>
#include <string.h>

int NET_initialize()
{
    return 0;
}

int NET_finalize()
{
    return 0;
}

int NET_get_last_error()
{
    return errno;
}

bool NET_is_error(int result)
{
    return result < 0;
}

bool NET_is_invalid(SOCKET socket)
{
    return socket < 0;
}

const char* NET_get_error_message(int code)
{
    return ::strerror(code);
}
