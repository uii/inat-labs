#include "NetCapture.h"
#include "printHexDump.h"
//==========================================================================
#include <functional>
#include <pcap.h>
#include <atomic>
#include <memory>
#include <thread>
#include <string.h>
#include <iostream>
#include <sstream>
//==========================================================================
const size_t BUFFER_SIZE = 1500;
//==========================================================================
void CaptureData(std::atomic_bool* captData, const NetCapture* netCapture);
void parsePacket(const char* data, const size_t dataLen);
//==========================================================================
template<class T>
T readPodType(const u_char* p)
{
	return inve(*reinterpret_cast<const T*>(p));
}

std::string ipToString(uint32_t ip)
{
	std::ostringstream ss;
	ss<<((ip&0xFF000000)>>24)<<'.';
	ss<<((ip&0x00FF0000)>>16)<<'.';
	ss<<((ip&0x0000FF00)>>8)<<'.';
	ss<<(ip&0x000000FF);
	return ss.str();
}
//===========================================================================

int main()
{
	std::string source;
	std::cout << "enter capture interface: ";
	std::cin >> source;
	bool sourceIsFile = false;
	pcap_t* src = openPcap(source, sourceIsFile);
	// Создаём фильтр пакетов:
	NetCapture netCapture(src);
	std::atomic_bool captDataThreadWork(true);
	std::thread captDataThread (CaptureData, &captDataThreadWork, &netCapture);
	// Теперь просто висим и ждём нажатия на клавишу. Выходим из приложения
	// по нажатию на 'q'.
	std::cout << "Press q to exit.." << std::endl;
	while (true) {
		char c;
		std::cin>>c;
		if (std::tolower(c)=='q')
			break;
	}
	captDataThreadWork = false;
	captDataThread.join();
	return 0;
}

void CaptureData(std::atomic_bool* captData, const NetCapture* netCapture)
{
	std::cout << "capture started" << std::endl;
	while (*captData) 
	{
		// Сюда мы будем бросать данные.
		const char* data(nullptr);
		// А здесь будет храниться их длина в байтах.
		int len(0);
		try {
			len = netCapture->getNextPacket(&data);
			if (len == -2){
				//Cлучай когда мы вышли по таймауту
				continue;
			}
			// Проверяем, не кончился ли файл. Если мы берём данные из live capture,
			// такого случая (len<0) быть не может.
			if (len<0)
				break;
		}
		catch (const ReadCaptureError& rce) {
			std::cout << "exception: " << rce.what() << std::endl;
		}
		// Пропускаем пустые пакеты:
		if (len==0) {
			continue;
		}
		
		try 
		{
			//Здесь надо обрабатывать перехваченные данные
			std::cout << "captured packet: len " << len << " data: " << std::endl;
			//Выводим перехваченный пакет на экран
			printHexDump(std::cout, data, len);
			std::cout << std::endl;
			parsePacket(data, len);

		}	
		catch (const std::exception& e) 
		{
			std::cout << "exception: " <<  e.what() << std::endl;
		}
	}
}

void parsePacket(const char* data, const size_t dataLen)
{
	//Разбираем заголовок пакета канального уровня
	const size_t ethernetHeaderLen = 14;
	uint16_t ipType = 0x0800;
	std::cout << "Ethernet frame header: ";
	printHexDump(std::cout, data, ethernetHeaderLen);
	std::cout << std::endl;
	std::cout << "destination mac adres: ";
	printHexDump(std::cout, data, 6);
	std::cout << std::endl;
	
	std::cout << "source mac adres: ";
	printHexDump(std::cout, data + 6, 6);
	std::cout << std::endl;
	uint16_t networkProtoNetOrder = 0;
	memcpy(&networkProtoNetOrder, &data[12], 2);
	uint16_t networkProto = inve(networkProtoNetOrder);
	std::cout << "network packet type ";
	if (networkProto != ipType)
	{
		std::cout << "unknow ";
		printHexDump(std::cout, data + 12, 2);
		std::cout << std::endl;
		return;
	}
	std::cout << "IP ";
	printHexDump(std::cout, data + 12, 2);
	std::cout << std::endl;

	//Разбираем IP пакет
	const uint8_t* ipHeader = (uint8_t*) data+ethernetHeaderLen;
	const int ipHeaderLen = (readPodType<uint8_t>(ipHeader+0x00)&0x0F)*4;
	const uint16_t totalLen = readPodType<uint16_t>(ipHeader+0x02);
	
	std::uint32_t srcIP = readPodType<uint32_t>(ipHeader+0x0C);
	std::uint32_t dstIP = readPodType<uint32_t>(ipHeader+0x10);

	int version = readPodType<uint8_t>(ipHeader);
	int ttl = readPodType<uint8_t>(ipHeader+0x08);
	uint8_t transportProtoType = readPodType<uint8_t>(ipHeader+0x09);
	const uint8_t udpType = 0x11;
	const uint8_t tcpType = 0x06;
	
	std::cout << "IP packet header: ";
	printHexDump(std::cout, ipHeader, ipHeaderLen);
	std::cout << std::endl;
	std::cout << "total packet len: " << totalLen <<  "; ip header len: " << ipHeaderLen 
		<< "; version code: " << version << "; ttl: " << ttl <<  ";" << std::endl;
	std::cout << "dst ip: " << ipToString(dstIP) << " ";
	printHexDump(std::cout, ipHeader+0x10, 4);
	std::cout << std::endl;
	std::cout << "src ip: " << ipToString(srcIP) << " ";
	printHexDump(std::cout, ipHeader+0x0C, 4);
	std::cout << std::endl;
	if (transportProtoType == tcpType)
	{	
		std::cout << "transport protocol type: tcp ";
		printHexDump(std::cout, ipHeader+0x09, 1);
		std::cout << std::endl;
		// Находим заголовок TCP.
		const uint8_t* tcpHeader = ipHeader+ipHeaderLen;
		const int tcpHeaderLen = ((readPodType<uint8_t>(tcpHeader+0x0C)&0xF0)>>4)*4;

		// Находим сами данные и их размер в байтах.
		// const uint8_t padding = header->caplen-ethernetLen-totalLen;
		const int tcpPayloadLen = totalLen-ipHeaderLen-tcpHeaderLen;
		uint16_t srcPort = readPodType<uint16_t>(tcpHeader+0x00);
		uint16_t dstPort = readPodType<uint16_t>(tcpHeader+0x02);
		std::cout << "source port: " << srcPort << "; destination port: " << dstPort 
		<< "; tcp header len: " <<  tcpHeaderLen << "; tcp payload len: " << tcpPayloadLen << ";" << std::endl;

		// Теперь получаем SEQ и ACK. Студентикам не помещает знать о них
		const uint32_t seq = readPodType<uint32_t>(tcpHeader+0x04);
		const uint32_t ack = readPodType<uint32_t>(tcpHeader+0x08);
		std::cout << "seq: " << seq << " ack: " << ack << std::endl;
		if (tcpPayloadLen)
		{
			const uint8_t* tcpPayload = tcpHeader+tcpHeaderLen;
			std::cout << "tcp payload:\n";
			printHexDump(std::cout, tcpPayload, tcpPayloadLen);
			std::cout << std::endl;
		}
	}
	else if (transportProtoType == udpType)
	{
		std::cout << "transport protocol type: udp ";
		printHexDump(std::cout, ipHeader+0x09, 1);
		std::cout << std::endl;
		// Находим заголовок UDP.
		const uint8_t* udpHeader = ipHeader+ipHeaderLen;
		uint16_t srcPort = readPodType<uint16_t>(udpHeader+0x00);
		uint16_t dstPort = readPodType<uint16_t>(udpHeader+0x02);
		uint16_t totalDatagramLen = readPodType<uint16_t>(udpHeader+0x04);
		int payloadDatagramLen = totalDatagramLen - 8;
		std::cout << "source port: " << srcPort << "; destination port: " << dstPort 
		<< "; total packet len: " << totalDatagramLen << "; payload len: " << payloadDatagramLen << ";" << std::endl;
		if (payloadDatagramLen)
		{
			const uint8_t* udpPayload = udpHeader + 8;
			std::cout << "udp payload:\n";
			printHexDump(std::cout, udpPayload, payloadDatagramLen);
			std::cout << std::endl;
		}
	}
	else
	{
		std::cout << "transport protocol type: unknow ";
		printHexDump(std::cout, ipHeader+0x09, 1);
		std::cout << std::endl;
		std::cout << "ip payload:\n";
		printHexDump(std::cout, ipHeader + ipHeaderLen, totalLen - ipHeaderLen);
		std::cout << std::endl;		
	}
}

