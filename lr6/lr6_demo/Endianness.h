#pragma once
#include <cstdint>
#include <cstddef>

inline std::uint8_t inve(const std::uint8_t& x)
{
	return x;
}

inline std::uint16_t inve(const std::uint16_t& x)
{
	return (x>>8) | (x<<8);
}

inline std::uint32_t inve(const std::uint32_t& x)
{
	return __builtin_bswap32(x);
}

inline std::uint64_t inve(const std::uint64_t& x)
{
	return __builtin_bswap64(x);
}

/*
template<class T>
T inve(const T& x)
{
	const unsigned char* bytes = reinterpret_cast<const std::uint8_t*>(&x);
	unsigned char result[sizeof(T)] = { 0 };
	for (std::size_t i=0; i<sizeof(T); ++i) {
		result[i] = bytes[sizeof(T)-i-1];
	}
	return *reinterpret_cast<T*>(&result);
}
*/

template<class T>
T inve(const T& x)
{
	const unsigned char* bytes = reinterpret_cast<const std::uint8_t*>(&x);
	//unsigned char result[sizeof(T)] = { 0 };
	T result(0);
	unsigned char* r = reinterpret_cast<unsigned char*>(&result);
	for (std::size_t i=0; i<sizeof(T); ++i) {
		r[i] = bytes[sizeof(T)-i-1];
	}
	return result;
}

