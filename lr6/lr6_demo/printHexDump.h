#include "iostream"
#include <iomanip>
//===================================================================

void printHexDump(std::ostream& os, const void* data, size_t datalen) //Метод для вывода буффера с байтами в некоторый поток вывода, например на экран или файл
{
    const unsigned char* p = reinterpret_cast<const unsigned char*>(data);
    for (size_t i=0; i<datalen; ++i) {
        os<<std::hex<<std::setfill('0')<<std::setw(2)<<static_cast<int>(p[i]);
        const int c = i+1;
        if (c%16==0)
            os<<'\n';
        else if (c%8==0)
            os<<"  ";
        else
            os<<' ';
    }
    os<<std::dec;
    os.flush();
}
