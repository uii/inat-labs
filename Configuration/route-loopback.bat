@echo off
setlocal EnableDelayedExpansion

rem ================== Agrument validation and usage hints ==================

set ENABLE=enable
set DISABLE=disable
set FOREVER=forever

set COMMAND=%1
if "%COMMAND%" == "%ENABLE%"  goto command_argument_checked
if "%COMMAND%" == "%DISABLE%" goto command_argument_checked
goto invalid_arguments
:command_argument_checked

set PERIOD=%2
if "%PERIOD%" == "" goto period_argument_checked
if "%PERIOD%" == "%FOREVER%" (
    if not "%COMMAND%" == "%ENABLE%" goto invalid_arguments
    set PERIOD=-p
    goto period_argument_checked
)
goto invalid_arguments
:period_argument_checked

goto arguments_checked

:invalid_arguments
echo "USAGE: %~nx0 {%ENABLE% [%FOREVER%]} | %DISABLE%"
exit /b 1
:arguments_checked


rem ============================= Configuration =============================

set LINE=1
for /f "tokens=2 delims=:" %%V in ('ipconfig') do (
    rem TODO: trim whitespace from the beginnings. 
    if "!LINE!" == "3" set ADDRESS=%%V
    if "!LINE!" == "5" set GATEWAY=%%V
    set /a LINE=!LINE!+1
)
for /f %%P in ('hostname') do set MACHINE=%%P

echo Machine:  %MACHINE%
echo Address: %ADDRESS%
echo Gateway: %GATEWAY%
echo Feature:  loopback traffic forwarding via default gateway

if "%COMMAND%" == "%ENABLE%" (
    route %PERIOD% add %ADDRESS% mask 255.255.255.255 %GATEWAY% metric 1
    echo Status:   enabled
) else (
    route delete %ADDRESS% %GATEWAY%
    echo Status:   disabled
)