@echo off

setlocal EnableDelayedExpansion

set action=%1
set count=%2
set port_start=%3
set port_shift=%4

for /L %%b in (1,1,%count%) do (
	echo Configuring rule %%b of %count%.
	set /a port_a=%port_start%+%%b
	set /a port_b=!port_a!+%port_shift%
	for %%p in (!port_a! !port_b!) do (
		for %%t in (TCP UDP) do (
			if "%action%" == "add" (
				set name="INaT %%b: %%p %%t"
			)
			netsh firewall %action% portopening %%t %%p !name! >NUL
		)
	)
)