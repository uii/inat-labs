@echo off
setlocal EnableDelayedExpansion

rem Get node index (computer name without the leading 'U').
for /f %%I in ('hostname') do set HOSTNAME=%%I
set NODE=!HOSTNAME:~1!

rem Read arguments (may be hardcoded for deployment).
set ACTION=%1
set PORT_START=%2
set PORT_SHIFT=%3

rem Create rules.
set /a PORT_A=%PORT_START%+!NODE!
set /a PORT_B=!PORT_A!+%PORT_SHIFT%
for %%P in (!PORT_A! !PORT_B!) do (
	for %%T in (TCP UDP) do (
		if "%action%" == "add" (
			set NAME="INaT !NODE!: %%P %%T"
		)
		netsh firewall %ACTION% portopening %%T %%P !NAME! >NUL
	)
)
