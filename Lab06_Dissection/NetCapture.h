#pragma once

#include "Endianness.h"
#include "../Common/networking.h"
#include <pcap.h>
#include <unordered_map>
#include <stdexcept>

#ifndef PCAP_NETMASK_UNKNOWN
#    define PCAP_NETMASK_UNKNOWN 0xffffffff
#endif

inline pcap_t* openPcap(const std::string& source, bool file)
{
	std::string errbuf(1024, '\0');
	pcap_t* src(nullptr);
	if (file)
		src = pcap_open_offline(source.c_str(), &errbuf[0]);
	else
		src = pcap_open_live(source.c_str(), 65535, 1, 1000, &errbuf[0]);

	if (src==nullptr)
		throw std::runtime_error("openPcap: couldn't open pcap source: "+errbuf);

	return src;
}

class ReadCaptureError : public std::runtime_error {
	public:
		ReadCaptureError(const std::string& error, int code) :
			std::runtime_error(error+" code="+std::to_string(code))
		{
		}
};

class NetCapture {
	private:
		template<class T>
		T readPod(const u_char* p) const
		{
			return inve(*reinterpret_cast<const T*>(p));
		}

		pcap_t* pcapSource_;
		struct bpf_program bpf_;
	public:
		enum class EtherType : uint16_t { IP=0x0800 };

		NetCapture(pcap_t* pcapSource, const std::string captureFilter="")
		:pcapSource_(pcapSource)
		{
			if (pcapSource==nullptr)
				throw std::invalid_argument("NetFilter::ctor: pcapSource is null");
			if (captureFilter != ""){
				if (pcap_compile(pcapSource_, &bpf_,
					captureFilter.c_str(), 1, PCAP_NETMASK_UNKNOWN)==-1) {
					throw std::invalid_argument("NetFilter::ctor: couldn't compile BPF filter `"+
						captureFilter + "`: " + std::string(pcap_geterr(pcapSource_)));
				}
				if (pcap_setfilter(pcapSource_, &bpf_)==-1) {
					throw std::runtime_error("NetFilter::ctor: couldn't install BPF filter `"+
						captureFilter + "`: " + std::string(pcap_geterr(pcapSource_)));
				}
			}
		}

		int getNextPacket(const char** result) const
		{
			pcap_pkthdr* header(nullptr);
			const u_char* data(nullptr);
			while (true) {
				// Сначала попробуем получить пакет. Будем тупо ждать, пока не получится.
				int r = pcap_next_ex(pcapSource_, &header, &data);
				switch (r) {
					case 1:
						break; // всё нормально
					case 0:
						return -2; // таймаут -- пробуем ещё раз
					case -1:
						throw ReadCaptureError("NetFilter::getNextPacket: read error", -1);
					case -2:
						return -1; // файл закончился -- выходим
					default:
						throw ReadCaptureError("NetFilter::getNextPacket: unknown result", r);
				}

				const uint8_t ethernetLen = 14; // длина заголовка Ethernet всегда 14 байтов.
				if (header->len<ethernetLen || !data) {
					continue;
				}
				// Будем смотреть только на IP-пакеты.
				if (EtherType(readPod<uint16_t>(data+0x0C))!=EtherType::IP)
					continue;

				*result = reinterpret_cast<const char*>(data);
				return header->len;
			}
		}
};

