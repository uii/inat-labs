#ifndef Solution_h__
#define Solution_h__

void parseEthernetFrame(const char* packet, const size_t length);
void parseIPPacket     (const char* packet, const size_t length);
void parseUDPPacket    (const char* packet, const size_t length);
void parseTCPPacket    (const char* packet, const size_t length);

#endif // Solution_h__
