#include "NetCapture.h"
#include "printHexDump.h"
#include "NetworkInterface.h"

#include <algorithm>
#include <functional>
#include <pcap.h>
#include <atomic>
#include <memory>
#include <thread>
#include <string.h>
#include <iostream>

#include "../Common/networking.h"

#include "solution.h"

void CaptureData(std::atomic_bool* captData, const NetCapture* netCapture);

std::string inputInterface();
void inputSource(
    int argc,
    const char* argv[],
    std::string& source,
    bool& source_is_file);

int main(int argc, const char* argv[])
try
{
    NET_initialize();

    std::string source;
    bool sourceIsFile;
    inputSource(argc, argv, source, sourceIsFile);

    pcap_t* src = openPcap(source, sourceIsFile);

	// Фильтры, могущие быть полезными:
	//      ""                      любые пакеты
	//      "udp"                   UDP
	//      "udp port 40000"        UDP через порт 40000
	//      "tcp dst port 40001"    входящие пакеты TCP на порт 40001
	std::string const filter = "";

	NetCapture netCapture(src, filter);

	// Создание и запуск потока, в котором выполняется перехват и разбор пакетов.
	std::atomic_bool captDataThreadWork(true);
	std::thread captDataThread (CaptureData, &captDataThreadWork, &netCapture);

	// Остановка фонового потока и выход из приложения после ввода 'q'.
	std::cout << "Enter 'q' to quit..." << std::endl;
	while (true) {
		char c;
		std::cin >> c;
		if (std::tolower(c) == 'q')
			break;
	}
	captDataThreadWork = false;
	captDataThread.join();

	NET_finalize();

	return 0;
}
catch (const std::exception& error) {
    std::cout << "Error in main thread:\n\t" << error.what() << std::endl;
} catch (...) {
    std::cout << "Error in main thread: details unknown." << std::endl;
}


// Обрабатывает поступающие пакеты и передает их функции разбора.
void CaptureData(std::atomic_bool* captData, const NetCapture* netCapture)
{
	std::cout << "Capture started." << std::endl;
	while (*captData)
	{
		// Указатель на массив, куда заносятся данные очередного пакета.
		// Управление памятью берет на себя NetCapture, new и delete не нужны.
		const char* data{nullptr};
		// Длина очередного пакета в байтах.
		int length{0};

		try {
			length = netCapture->getNextPacket(&data);
			// Cлучай истечения времени ожидания очередного пакета (не проблема).
			if (length == -2){
				continue;
			}
			// Проверка, не кончился ли файл (в случае разбора записи трафика).
			// Если данные из live capture, такого случая быть не может.
			if (length < 0)
				break;
		}
		catch (const ReadCaptureError& rce) {
			std::cout << "exception: " << rce.what() << std::endl;
		}
		// Пропуск пустых пакетов.
		if (length == 0) {
			continue;
		}

		try {
		    std::cout << "Packet of " << length << " bytes captured.\n";
			parseEthernetFrame(data, length);
			std::cout
                << "========================================================\n"
                << std::endl;
		}
		catch (const std::exception& e) {
			std::cout << "Exception: " <<  e.what() << std::endl;
		}
	}
}


// Получение источника source и проверка, является ли он файлом (source_is_file)
// из параметров командной строки или запросами у пользователя.
void inputSource(
        int argc,
        const char* argv[],
        std::string& source,
        bool& source_is_file)
{
    bool is_interactive = true;
    if (argc == 2) {
        is_interactive = false;
    } else if (argc != 1) {
        throw std::invalid_argument(
                "The program was given invalid arguments.\n"
                "Usage is as follows:\n"
                 "\tdissector\n"
                 "\tdissector device\n"
                 "\tdissector capture.pcap\n"
        );
    }

    if (is_interactive) {
        char answer;
        while (true) {
            std::cout << "Type `c' for live capture, `f' to load a dump file: ";
            std::cin >> answer;
            std::getline(std::cin, source);
            if (answer == 'c' || answer == 'f') {
                break;
            } else {
                std::cerr << "Please type `c' or `f'!" << std::endl;
            }
        }

        switch (answer) {
        case 'c':
            source = inputInterface();
            source_is_file = false;
            break;
        case 'f':
            std::cout << "Enter the dump filename: ";
            std::getline(std::cin, source);
            source_is_file = true;
        }
    } else {
        std::vector<NetworkInterface> const interfaces = getNetworkInterfaces();
        source = argv[1];
        auto iterator = std::find_if(
                interfaces.begin(),
                interfaces.end(),
                [&source](const NetworkInterface& item)
                    { return item.name == source; });
        source_is_file = interfaces.end() == iterator;
    }
}


// Печать таблицы доступных сетевых интерфейсов и выбор нужного пользователем.
std::string inputInterface()
{
    std::vector<NetworkInterface> const interfaces = getNetworkInterfaces();
    size_t const interface_count = interfaces.size();
    if (interface_count == 0) {
        throw std::runtime_error("no devices found in the system");
    }
    std::cout << interface_count << " device(s) available:\n";
    for (size_t i = 0; i < interfaces.size(); ++i) {
        std::cout << "Device # " << i << "\n"
                  << "\tName:    " << interfaces[i].name << "\n"
                  << "\tAddress: " << ::inet_ntoa(interfaces[i].address.sin_addr) << "\n";
    }

    size_t interface_index = 0;
    if (interface_count > 1) {
        do {
            std::cout << "Select device by index: ";
            std::cin >> interface_index;
            if (interface_index >= interface_count) {
                std::cout << "Please enter an integer!\n";
            }
        } while (interface_index >= interface_count);

    } else {
        std::cout << "Default interface selected. Press ENTER to start capture.\n";
        std::cin.get();
    }

    return interfaces[interface_index].name;
}

