#include <iostream>
#include <cstring>

#include "NetCapture.h"
#include "printHexDump.h"
#include "solution.h"

#define UNUSED(variable) (void)variable;

// Преобразует байты, начиная с `p', в значение типа `T', обращая порядок байт.
template<class T>
static T readPodType(const uint8_t* p)
{
	return inve(*reinterpret_cast<const T*>(p));
}


void parseEthernetFrame(const char* packet, const size_t length)
{
    UNUSED(length);

    std::cout << "-------------------- Ethernet Frame --------------------\n";

    // Размер заголовка кадра и размер MAC-адреса берется из справочников.
    size_t const ETHERNET_HEADER_SIZE = 14;
    size_t const MAC_ADDRESS_SIZE = 6;

    std::cout << "Destination MAC: ";
    printHexDump(std::cout, packet + 0, MAC_ADDRESS_SIZE);
    std::cout << '\n';
    std::cout << "Source MAC:      ";
    printHexDump(std::cout, packet + 6, MAC_ADDRESS_SIZE);
    std::cout << '\n';

    // Необходимо следить за порядком байт. Двухбайтовый код протокола сетевого
    // уровня, пакет которого передается в кадре Ethernet, в заголовке кадра
    // хранится в сетевом порядке байт. Поэтому нужно преобразование в порядок
    // байт данной машины.
    uint16_t protocol_code_in_network_byte_order;
    memcpy(&protocol_code_in_network_byte_order, packet+12, sizeof(uint16_t));
    // Другой способ, без копирования:
    protocol_code_in_network_byte_order =
        *reinterpret_cast<const uint16_t*>(&packet[12]);
    uint16_t const protocol_code = ::ntohs(protocol_code_in_network_byte_order);
    // То же самое с использованием функции:
    //      const auto protocol_code = readPodType<uint16_t>(&packet[12]);

    // Коды протоколов (здесь: протокола IP) берутся из справочников.
    uint16_t const IP_CODE = 0x0800 /*IPPROTO_IP*/;

    if (protocol_code == IP_CODE) {
        const char* payload = packet + ETHERNET_HEADER_SIZE;
        size_t const payload_length = length - ETHERNET_HEADER_SIZE;

        // TODO: печать содержимого пакета канального уровня показана
        //       для примера; следует этот фрагмент удалить.
        std::cout << "Payload:\n";
        printHexDump(std::cout, payload, payload_length);
        std::cout << '\n';

        parseIPPacket(payload, payload_length);
    } else {
        std::cout << "Unknown network layer protocol code: "
                  << protocol_code << '\n';
    }
}


void parseIPPacket(const char* packet, const size_t length)
{
    UNUSED(packet);
    UNUSED(length);

    std::cout << "---------------------- IP Packet -----------------------\n";

    // TODO: здесь следует разобрать заголовок пакета IP и напечатать нужное.

    // TODO: полную длину пакета нужно извлечь из заголовка пакета.
    size_t const total_length = 0;

    // TODO: длину заголовка нужно извлечь из самого заголовка пакета.
    size_t const header_length = 0;

    // TODO: это значение нужно извлечь из заголовка пакета.
    uint16_t const transport_protocol = 0x0000;

    const char* payload = packet + header_length;
    size_t const payload_length = total_length - header_length;
    switch (transport_protocol) {
    case IPPROTO_UDP:
        parseUDPPacket(payload, payload_length);
        break;
    case IPPROTO_TCP:
        parseTCPPacket(payload, payload_length);
        break;
    default:
        std::cout << "Unknown transport layer protocol code: "
                  << transport_protocol << '\n';
        break;
    }
}


void parseUDPPacket(const char* packet, const size_t length)
{
    UNUSED(packet);
    UNUSED(length);

    std::cout << "---------------------- UDP Packet ----------------------\n";

    // TODO: здесь следует разобрать заголовок пакета UDP и напечатать нужное.
}


void parseTCPPacket(const char* packet, const size_t length)
{
    UNUSED(packet);
    UNUSED(length);

    std::cout << "---------------------- TCP Packet ----------------------\n";

    // TODO: здесь следует разобрать заголовок пакета TCP и напечатать нужное.
}

