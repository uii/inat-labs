#ifndef NetworkInterface_h_INCLUDED
#define NetworkInterface_h_INCLUDED

#include <vector>
#include <string>

#include "../Common/networking.h"

#include <pcap.h>

// Сетевой интерфейс (адаптер, NIC).
struct NetworkInterface
{
    // Системное наименование, нужно для openPcap().
    std::string name;

    // Первый адрес IP, назначенный интерфейсу (и обычно единственный).
    sockaddr_in address;
};


// Возвращает список сетевых интерфейсов с их основными адресами IPv4.
inline std::vector<NetworkInterface> getNetworkInterfaces()
{
    pcap_if_t *all_devices;

	std::string error_message(PCAP_ERRBUF_SIZE + 1, '\0');
	if (pcap_findalldevs(&all_devices, &error_message[0]) == -1) {
		throw std::runtime_error("pcap_findalldevs: " + error_message);
	}

    std::vector<NetworkInterface> interfaces;
	for (const pcap_if_t *device = all_devices; device; device = device->next) {
		for (const pcap_addr_t* address = device->addresses; address; address = address->next) {
            if (address && address->addr && address->addr->sa_family == AF_INET) {
                NetworkInterface current;
                current.name = device->name;
                current.address = *reinterpret_cast<sockaddr_in*>(address->addr);
                interfaces.push_back(std::move(current));
                break;
            }
		}
	}

	pcap_freealldevs(all_devices);

	return interfaces;
}

#endif // NetworkInterface_h_INCLUDED
