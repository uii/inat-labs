#include <stdexcept>

#include "ClientSocket.h"
#include "PacketProtocol.h"

PacketProtocol::PacketProtocol(ClientSocket& channel) :
    channel_{channel}
{
}

void PacketProtocol::receiveSome(void* data, size_t size)
{
    auto bytes = reinterpret_cast<char*>(data);
    size_t bytes_received = 0;
    while (bytes_received != size) {
        size_t const received_now = channel_.receive(
                bytes + bytes_received, size - bytes_received);
        if (!received_now) {
            throw std::logic_error("peer disconnected unexpectedly");
        }
        bytes_received += received_now;
    }
}

void PacketProtocol::sendSome(const void* data, size_t size)
{
    auto bytes = reinterpret_cast<const char*>(data);
    size_t bytes_sent = 0;
    while (bytes_sent != size) {
        bytes_sent += channel_.send(bytes + bytes_sent,
                                    size  - bytes_sent);
    }
}

MemoryBlock PacketProtocol::receivePacket()
{
    uint64_t size;
    receiveSome(&size, sizeof(size));
    MemoryBlock packet(size);
    receiveSome(packet.getData(), size);
    return packet;
}

void PacketProtocol::sendPacket(const MemoryBlock& packet)
{
    const uint64_t size = packet.getSize();
    sendSome(&size, sizeof(size));
    sendSome(packet.getData(), size);
}
