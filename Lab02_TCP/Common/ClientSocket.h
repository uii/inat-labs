#ifndef CLIENTSOCKET_H_INCLUDED
#define CLIENTSOCKET_H_INCLUDED

#include "StreamSocket.h"

// Сокет клиента, то есть сокет-передатчик.
class ClientSocket : public StreamSocket
{
public:
    // Создает новый потоковый сокет-передатчик.
    ClientSocket();

    // Создает объект на основе имеющегося сокета (например, результата accept()).
    ClientSocket(SOCKET channel);

    // Устанавливает соединение с сервером по указанному адресу и порту.
    void connect(const char* host, unsigned short port);

    // Разрывает соединение с сервером.
    void disconnect();

    // Удобные вызовы ::send() и ::recv() с  проверкой ошибок.
    size_t send(const void* data, size_t size);
    size_t receive(void* data, size_t size);
};

#endif // CLIENTSOCKET_H_INCLUDED
