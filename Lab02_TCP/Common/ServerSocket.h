#ifndef SERVERSOCKET_H_INCLUDED
#define SERVERSOCKET_H_INCLUDED

#include "ClientSocket.h"
#include "StreamSocket.h"

// Сокет сервера, то есть сокет-слушатель входящих подключений.
class ServerSocket : public StreamSocket
{
public:
    // Создает сокет, который будет принимать подключения на заданный адрес и порт.
    ServerSocket(const char* host, unsigned short port);

    // Выполняет подключение клиента и возвращает подключенного.
    // Заполняет адрес клиента, если аргумент передан.
    ClientSocket accept(sockaddr_in* remote_address = nullptr);
};

#endif // SERVERSOCKET_H_INCLUDED
