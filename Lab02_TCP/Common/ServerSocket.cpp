#include "../../Common/NetworkError.h"

#include "ServerSocket.h"

ServerSocket::ServerSocket(const char* host, unsigned short port) :
    StreamSocket()
{
    sockaddr_in const address = make_endpoint(host, port);
    int const result = ::bind(channel_, (const sockaddr*)&address, sizeof(address));
    if (NET_is_error(result)) {
        RAISE_NETWORK_ERROR();
    }
}


ClientSocket ServerSocket::accept(sockaddr_in* remote_address/*=nullptr*/)
{
    int const result = ::listen(channel_, SOMAXCONN);
    if (NET_is_error(result)) {
        RAISE_NETWORK_ERROR();
    }

    SOCKET client;
    if (remote_address) {
        socklen_t address_size = sizeof(sockaddr_in);
        client = ::accept(channel_, (sockaddr*)remote_address, &address_size);
    } else {
        client = ::accept(channel_, nullptr, nullptr);
    }
    if (NET_is_invalid(client)) {
        RAISE_NETWORK_ERROR();
    }
    return client;
}
