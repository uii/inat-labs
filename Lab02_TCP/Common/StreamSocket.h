#ifndef STREAMSOCKET_H
#define STREAMSOCKET_H

#include "../../Common/networking.h"

// Потоковый сокет.
// Автоматически уничтожает сокет, служит базовым классом для сокетов клиента и сервера.
class StreamSocket
{
public:
    StreamSocket();

    // Нельзя копировать сокет: это уникальный объект ОС.
    StreamSocket(const StreamSocket&) = delete;
    StreamSocket& operator=(const StreamSocket&) = delete;

    // Сокет можно перемещать  (например, возвращать из функций).
    StreamSocket(StreamSocket&& source);
    StreamSocket& operator=(StreamSocket&& source);

    // Закрывает соединение и уничтожает системный сокет.
    void close();

    // Уничтожает системный сокет автоматически.
    virtual ~StreamSocket();

    // Позволяет использовать объект StreamSocket как SOCKET.
    operator SOCKET() const;

protected:
    SOCKET channel_;
};

#endif // STREAMSOCKET_H
