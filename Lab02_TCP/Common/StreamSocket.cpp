#include "../../Common/NetworkError.h"

#include "StreamSocket.h"

StreamSocket::StreamSocket()
{
    channel_ = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (NET_is_invalid(channel_)) {
        RAISE_NETWORK_ERROR();
    }
}

StreamSocket::~StreamSocket()
{
    close();
}

void StreamSocket::close()
{
    if (channel_ && !NET_is_invalid(channel_)) {
        ::closesocket(channel_);
        channel_ = 0;
    }
}

StreamSocket::operator SOCKET() const
{
    return channel_;
}
