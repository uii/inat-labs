#ifndef PROTOCOL_H_INCLUDED
#define PROTOCOL_H_INCLUDED

// Тип пакета. Конструкция `enum class' означает, что обращаться к константам
// следует как Type::Request и т.п., а не просто Request. Размер типа Type
// равен размеру char (1 байт).
enum class Type : char
{
    Request = 'R',
    File = 'F',
    Error = 'E',
    Quit = 'Q'
};

#endif // PROTOCOL_H_INCLUDED
