#include "../../Common/NetworkError.h"

#include "ClientSocket.h"

ClientSocket::ClientSocket() :
    StreamSocket()
{
}


ClientSocket::ClientSocket(SOCKET channel)
{
    channel_ = channel;
}


size_t ClientSocket::send(const void* data, size_t size)
{
    auto bytes = reinterpret_cast<const char*>(data);
    int const result = ::send(channel_, bytes, size, 0);
    if (NET_is_error(result)) {
        RAISE_NETWORK_ERROR();
    }
    return result;
}


size_t ClientSocket::receive(void* data, size_t size)
{
    auto bytes = reinterpret_cast<char*>(data);
    int const result = ::recv(channel_, bytes, size, 0);
    if (NET_is_error(result)) {
        RAISE_NETWORK_ERROR();
    }
    return result;
}


void ClientSocket::connect(const char* host, unsigned short port)
{
    sockaddr_in const address = make_endpoint(host, port);
    int const result = ::connect(channel_, (const sockaddr*)&address, sizeof(address));
    if (NET_is_error(result)) {
        RAISE_NETWORK_ERROR();
    }
}


void ClientSocket::disconnect()
{
    ::shutdown(channel_, SD_BOTH);
}
