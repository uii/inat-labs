#ifndef PACKETPROTOCOL_H_INCLUDED
#define PACKETPROTOCOL_H_INCLUDED

#include "../../Common/MemoryBlock.h"

// Протокол прикладного уровня, пакеты которого состоят
// из размера (4 байта) и данных указанного размера.
class PacketProtocol final
{
public:
    PacketProtocol(class ClientSocket& channel);

    // Принимает пакет и возвращает его содержимое (без длины).
    MemoryBlock receivePacket();

    // Отправляет длину и указанное содержимое пакета.
    void sendPacket(const MemoryBlock& packet);

private:
    // Принимают и отправляют фиксированное size количество байт.
    void receiveSome(void* data, size_t size);
    void sendSome(const void* data, size_t size);

private:
    class ClientSocket& channel_;
};

#endif // PACKETPROTOCOL_H_INCLUDED
