#include <cctype>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

#include "../../Common/networking.h"
#include "../../Common/NetworkError.h"
#include "../../Common/MemoryBlock.h"

#include "../Common/ClientSocket.h"
#include "../Common/PacketProtocol.h"
#include "../Common/protocol.h"


int main()
try
{
    NET_initialize();

    char address[INET_ADDRSTRLEN];
    std::cout << "Enter server address: ";
    std::cin >> address;
    unsigned short port;
    std::cout << "Enter server port: ";
    std::cin >> port;

    ClientSocket channel;
    channel.connect(address, port);

    PacketProtocol client(channel);

    while (true) {
        std::cout << "Enter filename: ";
        std::string request;
        std::cin >> request;
        if (request == "/quit") {
            MemoryBlock goodbye;
            goodbye.append(Type::Quit);
            client.sendPacket(goodbye);
            break;
        }

        MemoryBlock packet;
        packet.append(Type::Request);
        packet.appendString(request);

        client.sendPacket(packet);

        packet = client.receivePacket();

        size_t position = 0;
        const auto type = packet.read<Type>(position);

        switch (type) {
        case Type::File: {
            const char* content = packet.readString(position);
            const size_t size = packet.getSize() - sizeof(type);

            std::cout << "OK, received " << size << " bytes." << std::endl;
            char answer;
            std::cout << "Would you like to (p)rint or to (s)ave file? ";
            std::cin >> answer;
            answer = ::tolower(answer);

            if (answer == 'p') {
                std::cout.write(content, size);
                std::cout << std::endl;
            } else if (answer == 's') {
                // Имя файла --- это часть пути за последней косой чертой
                // (в Windows, возможно, обратной, а не прямой).
                const char* filename =
                    request.c_str() + request.find_last_of("/\\") + 1;
                std::ofstream file(filename, std::ios::out | std::ios::binary);
                file.write(content, size);
                file.close();
            } else {
                std::cerr << "Hey!" << std::endl;
            }
            } break;

        case Type::Error: {
            const char* message = packet.readString(position);
            std::cerr << "FAIL: " << message << std::endl;
            } break;

        default:
            std::cerr << "Unknown server response type!" << std::endl;
        }
    }

    channel.disconnect();

    NET_finalize();
    return 0;
}
catch (const NetworkError& error)
{
    std::cerr << "Network ERROR "
              << error.getCode() << ": " << error.getMessage() << '\n'
              << "AT " << error.getFile() << ": " << error.getLine() << '\n'
              << "IN " << error.getPlace() << std::endl;
}
catch (const std::exception& error)
{
    std::cerr << "ERROR: " << error.what() << std::endl;
}
