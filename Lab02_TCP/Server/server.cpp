#include <cctype>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>

#include "../../Common/networking.h"
#include "../../Common/NetworkError.h"
#include "../../Common/MemoryBlock.h"

#include "../Common/ClientSocket.h"
#include "../Common/ServerSocket.h"
#include "../Common/PacketProtocol.h"
#include "../Common/protocol.h"


void processClient(ClientSocket& channel);
void handleErrors();


int main()
try
{
    NET_initialize();

    char address[INET_ADDRSTRLEN];
    std::cout << "Enter address: ";
    std::cin >> address;
    unsigned short port;
    std::cout << "Enter port: ";
    std::cin >> port;

    ServerSocket listener(address, port);

    while (true) {
        std::clog << "Waiting for incoming connections..." << std::endl;
        sockaddr_in remote_address;
        ClientSocket channel = listener.accept(&remote_address);

        std::clog << "Client connected from "
                  << ::inet_ntoa(remote_address.sin_addr) << ":"
                  << ::ntohs(remote_address.sin_port)     << "." << std::endl;
        processClient(channel);
        std::clog << "Client disconnected." << std::endl;
    }

    NET_finalize();
    return 0;
}
catch (...)
{
    handleErrors();
}


void processClient(ClientSocket& channel)
try
{
    PacketProtocol client(channel);

    while (true) {
        MemoryBlock packet;
        try {
            packet = client.receivePacket();
        } catch (const std::logic_error& error) {
            std::clog << "WARNING: " << error.what() << std::endl;
            return;
        }

        size_t position = 0;
        const auto type = packet.read<Type>(position);

        switch (type) {
        case Type::Request: {
            const char* path = packet.readString(position);

            MemoryBlock response;

            std::clog << "Requested for `" << path << "'... ";
            std::ifstream file(path, std::ios::in | std::ios::binary);
            if (file) {
                file.seekg(0, std::ios::end);
                size_t const size = file.tellg();
                file.seekg(0);
                std::clog << "found (" << size << " bytes)." << std::endl;

                response.append(Type::File);
                response.growBy(size);
                file.read(response.getData() + sizeof(Type), size);
            } else {
                std::clog << "missing!" << std::endl;

                response.append(Type::Error);
                response.appendString("unable to access the file requested");
            }

            std::clog << "Sending response... ";
            client.sendPacket(response);
            std::clog << "done." << std::endl;

            } break;

        case Type::Quit: {
            channel.close();
            return;
            } break;

        case Type::Error: {
            std::clog << "Unexpected `Error' packet received!" << std::endl;
            } break;

        default:
            std::clog << "Unknown client request type `" << (char)type << "'!" << std::endl;
        }
    }
}
catch (...)
{
    handleErrors();
}


// Общая часть, связанная с обработкой ошибок.
void handleErrors()
try
{
    throw;
}
catch (const NetworkError& error)
{
    std::cerr << "Network ERROR "
              << error.getCode() << ": " << error.getMessage() << '\n'
              << "AT " << error.getFile() << ": " << error.getLine() << '\n'
              << "IN " << error.getPlace() << std::endl;
}
catch (const std::exception& error)
{
    std::cerr << "ERROR: " << error.what() << std::endl;
}
